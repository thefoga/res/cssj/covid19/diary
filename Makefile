cwd            := $(shell pwd)
bib_builder    := biber
pdf_builder    := pdflatex
html_builder   := htlatex
docx_builder   := libreoffice --headless --convert-to "docx:Office Open XML Text"
viewer         := xdg-open
editor         := /usr/bin/vim
input_file     := diary
out_folder     := "$(HOME)/Dropbox/Carlo and SSJ/stefano/"

.PHONY: all view

view:
	$(viewer) $(input_file).pdf

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof *.dvi *.bcf *.run* # root
	rm -fv */*.aux */*.log */*.bbl */*.blg */*.toc */*.out */*.lot */*.lof */*.dvi */*.bcf */*.run*
	rm -fv */*/*.aux */*/*.log */*/*.bbl */*/*.blg */*/*.toc */*/*.out */*/*.lot */*/*.lof */*/*.dvi */*/*.bcf */*/*.run*
	rm -fv *.4ct *.4tc *.css *.html *.idv *.lg *.tmp *.xref *.png

compile::
	$(pdf_builder) $(input_file)

build::
	$(MAKE) clean
	$(MAKE) compile
	$(bib_builder) $(input_file)
	$(MAKE) compile
	$(MAKE) compile

html::
	$(html_builder) $(input_file)

docx::
	$(MAKE) html
	$(docx_builder) $(input_file).html

all::
	$(MAKE) build
	$(MAKE) docx

publish::
	$(MAKE) all
	cp $(input_file).docx $(out_folder) && cp $(input_file).pdf $(out_folder)

git::
	$(MAKE) build
	git add --all
	git commit -m "Auto-generated push for backup reasons. See previous commits for meaningful message."
	git push

debug::
	$(MAKE) build
	$(MAKE) view

edit::
	$(editor) $(input_file).tex
