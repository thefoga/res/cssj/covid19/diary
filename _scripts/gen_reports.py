#!/usr/bin/env python

from pathlib import Path
from hal.times.dates import Weekday
from datetime import datetime, timedelta


class Report():
    def __init__(self, date_time):
        self.d = date_time

    def get_text(self):
        nice_date = self.d.strftime('%B %d, %Y')
        label_date = self.d.strftime('%Y-%m-%d')
        out = '\\section{{\\report{{}} {}}}\n\\label{{{}-section}}\n'
        return out.format(nice_date, label_date)

    def get_filename(self):
        date_time = self.d.strftime('%Y-%m-%d')
        return '{}.tex'.format(date_time)

    def get_include(self, folder):
        date_time = self.d.strftime('%Y-%m-%d')
        return '\\include{{{}/{}}}'.format(folder, date_time)

    def generate(self, folder):
        full_path = Path(folder).joinpath(self.get_filename())
        with open(full_path, 'w') as out:
            out.write(self.get_text())

    def __str__(self):
        return self.d.strftime('%Y-%m-%d')

class Diary():
    def __init__(self, report_weekday, reports_folder):
        self.report_weekday = report_weekday
        self.reports_folder = reports_folder

    def get_next_report_date(self, current_date):
        return current_date + timedelta(days=7)  # 1 week

    def get_next_reports(self, until):
        reports = []
        report_date = Weekday.get_next(self.report_weekday)  # assuming reports until now are done

        while report_date < until:
            report = Report(report_date)
            reports.append(report)
            report_date = self.get_next_report_date(report_date)

        return reports

    def generate_reports(self, until):
        reports = self.get_next_reports(until)
        for report in reports:
            report.generate(self.reports_folder)
            print('{} done!'.format(str(report)))  # debug only

    def generate_includes(self, until):
        reports = self.get_next_reports(until)
        for report in reports:
            print(report.get_include('reports'))

def main():
    last_report = datetime.strptime('2020-08-30', '%Y-%m-%d')
    reports_folder = Path('~/Scuola/tud/_classes/2/team-proj/diary/reports/').expanduser()

    diary = Diary(Weekday.FRIDAY, reports_folder)
    diary.generate_reports(last_report)
    diary.generate_includes(last_report)

if __name__ == '__main__':
    main()
